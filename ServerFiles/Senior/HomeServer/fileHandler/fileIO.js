var fs = require('fs');
const log = require('../loggingModule/log');








/**
* @method initRead reads list of known clients from clientsList resource and populates clients array
* @param {object} socket - socket object of the connecting client
* @param {Array} clients - list of all clients connected to server representing Rooms
* @param {string} clientID - the clientID of the incomming connection
**/
function initRead(callback) {

  file_read_clients(callback);

}



function read_from_file(){}
function write_to_file(){}




function file_read_clients(callback){

  var path = __dirname + '/clientsList.txt';

      fs.readFile(path, function (err, data) {
        if (err) throw err;
        callback(data);

      });




}
function file_write_clients(data){
  var path = __dirname + '/clientsList.txt',
  buffer = new Buffer(data);
  log.file_log(buffer);
  fs.open(path, 'a', function(err, fd) {
      if (err) {
          throw 'error opening file: ' + err;
      }

      fs.write(fd, buffer, 0, buffer.length, null, function(err) {
          if (err) throw 'error writing file: ' + err;
          fs.close(fd, function() {})
      });
  });

}


module.exports = {file_write_clients:file_write_clients,file_read_clients:file_read_clients,initRead:initRead};
