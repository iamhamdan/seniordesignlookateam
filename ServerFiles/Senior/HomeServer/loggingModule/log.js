var fs = require('fs');

function file_log(data){

  var path = __dirname + '/logfile.txt',
  buffer = new Buffer(new Date+"        " + data + "\n");

  fs.open(path, 'a', function(err, fd) {
      if (err) {
          throw 'error opening file: ' + err;
      }

      fs.write(fd, buffer, 0, buffer.length, null, function(err) {
          if (err) throw 'error writing file: ' + err;
          fs.close(fd, function() {

          })
      });
  });

}



module.exports = {file_log:file_log};
