const firebase = require('firebase');
const log = require('../loggingModule/log');
var apiKey,authDomain,databaseURL





function init(apiKey='AIzaSyDOoZGMnl6CLZ3Np7FR6Gi9nl-RhjuTteU',authDomain='universal-smart-house.firebaseapp.com',databaseURL='https://universal-smart-house.firebaseio.com/' ){
  this.apiKey = apiKey;
  this.authDomain = authDomain;
  this.databaseURL = databaseURL;
  console.log(apiKey);

  const firebaseConfig = {
      apiKey: apiKey,
      authDomain: authDomain,
      databaseURL: databaseURL

  };
  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
}



/**
* @method readFromDB determines what function to call based on what is to be read from DB... main read entry point into module from outside
**/
function readFromDB(username,cid,cmd,callback=0){
  console.log(username);
  console.log(cid);

  switch (cmd['todo']) {
    case "check Assigned":
      checkAssigned(username,cid,cmd,callback);

      break;
    default:
  }
}

/**
* @method checkAssigned checks if room with given ID is unassigned
**/
function checkAssigned(username,cid,cmd,callback){//reads Rooms node from client ID firebase DB
  console.log("checking Assigned");
  firebase.database().ref('/' + username + '/Rooms').once('value').then(function(snapshot) {
    try{
      if (snapshot === null || snapshot === undefined) {
        log.file_log("||module firebaseWrapper||ERROR: username not found in database.");
        console.log("ERROR: username not found in database.");
        process.exit(1);
      //  throw new Error("Null snapshot Exception");
      }
      //console.log(JSON.stringify(snapshot, null, 4));
       log.file_log("||module firebaseWrapper|| reading data firebase of client: " +cid);
       snapshot = JSON.parse(JSON.stringify(snapshot, null, 4));
       console.log(snapshot.unassigned);
       var unassigned = false;
       for(var itterator in snapshot.unassigned)
       {
         if (snapshot.unassigned[itterator]===(cid)) {
           unassigned = true;


         }
       }
       if(unassigned)
       {
        console.log("Room is unassigned");
       }
       else{
          console.log(snapshot);
         for (var itterator in snapshot) {
           if(snapshot[itterator].isRoom==1){
             if (snapshot[itterator].id == cid) {
               console.log(snapshot[itterator]);
             }
           }
         }
       }

    }
    catch(e)
    {
      console.log(e);
    }



     //console.log(JSON.stringify(snapshot, null, 4))

     //cmd['data'].write(JSON.stringify(snapshot));//##WRITE CORRECT FORMAT  wait no need to write


   });

}
function readData(clientID){
  firebase.database().ref('/' + clientID + '/').once('value').then(function(snapshot) {

     console.log(JSON.stringify(snapshot, null, 4))
     log.file_log("||module firebaseWrapper|| reading data firebase of client: " +clientID);
     log.file_log(JSON.stringify(snapshot, null, 4))


   });
}

function writeUnassigned(){
  //UID -> Unassigned


}

function attachListeners(rooms){
  houseListeners[rooms.length] = firebase.database().ref(userID + "/MainDoorLock" );
  houseListeners[rooms.length].on('value', function(snapshot) {
  var printer = snapshot.key;

  console.log("Door changed");
  console.log(snapshot.val());
  });

  for (room in rooms){
    var path = userID + '/Rooms' + '/'+ rooms[room]
    console.log(path);
    houseListeners[room] = firebase.database().ref(path );
    houseListeners[room].on('value', function(snapshot) {
    var printer = snapshot.key;

    console.log(printer + " is the one that changed");
    console.log(snapshot.val());
    });

  }
}



module.exports= {init:init,attachListeners:attachListeners, readFromDB:readFromDB};
