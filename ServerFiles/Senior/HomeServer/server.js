

const firebase = require('./firebaseModule/firebaseWrapper');
const log = require('./loggingModule/log');
const FIO = require('./fileHandler/fileIO');
const net = require('net');
const connectionHandler = require("./connectionHandler/connectionHandler");
var username = 'UID';
var clients = [];



if (process.argv.length <= 2) {
	console.log("Error: username not valid. Please enter username as such - node .\\server.js {USERNAME}");
	log.file_log("||main server||Error: username not valid/was not entered.");
	process.exit(1);

}
 else {

	 username = process.argv[2];

 }



function setClients(stringToSplit){
	log.file_log("||main server||Server setup initiating... reading clientsList");
	stringToSplit = stringToSplit.toString('utf8');
	stringToSplit = stringToSplit.replace(/\n/g, "",-1);
	separator = '.';
	var arrayOfStrings = stringToSplit.split(separator);
	//	console.log('The array has ' + arrayOfStrings.length + ' elements: ' + arrayOfStrings.join(' / '));
	for(var itterator in arrayOfStrings){
		var stringID = arrayOfStrings[itterator];
		//if statement to check if its an empty line'
		if ( !isEmptyObject(stringID)) {
			var client = {
				cid: stringID,
				socket : null
			};
			clients.push(client);
		}
	}
log.file_log("||main server||Server setup completed successfully... ready for data");

}

function getClientsArray(){
	return clients;
}
function setClientsArray(clientsTemp){
	clients = clientsTemp;
}



FIO.initRead(setClients);
firebase.init();
var server = net.createServer(function(socket) {
	socket.on('data', function (data) {
		log.file_log("data received by server : " + data);

		//try {//attempt to parse the string received into a json object
			var obj = JSON.parse(data.toString());
			var flags =obj['flags'];
			var cid = obj['cid'];
			//valid data condition -> check for clientID and flags .. if not present, data considered invalid and not processed.
			if (flags != null && cid!=null) {
				//newly connected client no ID yet

				var clientsTemp = getClientsArray();
				if(connectionHandler.clientHandler(socket,clientsTemp,cid,flags))
				{
					cmd = {todo: "check Assigned",data:socket};

					//if false returned, it was a new client setting up still.
					firebase.readFromDB(username,cid,cmd);
					//takes snapshot
				}
				else {
				}
				setClientsArray(clientsTemp);
			}
			//invalid data condition
			else {
				log.file_log("||main server||invalid data received by server, no ID field: " + data);
			}
	//	}
		//catch(e) {
			//var string = data.toString();

			//log.file_log("||main server||invalid data received by server, invalid format: " + e );
		//}
	});

	socket.on('close', function(data) {
		log.file_log("||main server|| a client connection has terminated");
	});
	socket.on('error', function(data) {
		log.file_log('||main server||error:-> '+data);

	});
});
server.on('close', function(){

	log.file_log("||main server||server closed");
});
server.listen(1337, '127.0.0.1');

//firebase.init();
//firebase.attachListeners(rooms);



/**
* checks if a json object is empty.
* @method isEmptyObject
* @param {object} object - any JSON object
* @return {Boolean} returns true if object has an entry, false if object is empty.
**/
function isEmptyObject(object) {
  for (var key in object) {
    if (Object.prototype.hasOwnProperty.call(object, key)) {
      return false;
    }
  }
  return true;
}
