const randomstring = require("randomstring");
const log = require('../loggingModule/log');
const FIO = require('../fileHandler/fileIO');
var client = {
  socket : null,
  port : null,
  ip: ''
};



/**
* @method clientHandler
* @param {object} socket - socket object of the connecting client
* @param {Array} clients - list of all clients connected to server representing Rooms
* @param {string} cid - the cid of the incomming connection
**/
function clientHandler(socket,clients,cid,flags){
  switch (cid) {
    case '0':
    return newClientHandler(socket,clients);
    return false;
    break;
    case 'adminClient':

    break;
    default:return returningClientHandler(socket,clients,cid,flags);

  }
}
/**
* Generates a new clintID of length 16, fully uppercase. Creates a new client object with the generated ID, socket, port and ip and adds it to the
* array of clients. Proceeds to forcefully terminate the socket connection in order to reconnect to the server
* @method newClientHandler
* @param {object} socket - socket object of the connecting client
* @param {Array} clients - list of all clients connected to server representing Rooms
* @return {Boolean} returns false and closes socket
**/
function newClientHandler(socket,clients){//NOT TESTED
  var generatedID = randomstring.generate({
    charset: 'hex',
    length: '5',
    capitalization : 'uppercase'
  });
  log.file_log("generated ID = " + generatedID);//check PLEASE


  socket.write(generatedID);
  var client = {
    cid: generatedID,
    socket : socket
  };
  while(!checkUniqueID(generatedID,clients)){
    var generatedID = randomstring.generate({
      charset: 'hex',
      length: '5',
      capitalization : 'uppercase'
    });
  }
  clients.push(client);

  socket.end();
  return false;


}
/**
* Finds the client through the cid by iterating through the list of clients and returns when found, if the client was not found
* in the list, the socket connection is terminated and an error is logged
* @method returningClientHandler
* @param {object} socket - socket object of the connecting client
* @param {Array} clients - list of all clients connected to server representing Rooms
* @param {string} cid - the cid of the incomming connection
* @return {Boolean} returns true if client ID found within clients list, false if ID is invalid
**/
function returningClientHandler(socket,clients,cid,flags) {
  if (checkUniqueID(cid,clients)) {//if ID is found within clients array it means ID is a valid room, you may procede.
    socket.end();
    log.file_log("ERROR: client with ID " + cid + " Attempting to talk to server with no valid ID, terminating connection");
    return false;
  }

  return true
}



/**
* @method checkUniqueID
* @param {string} generatedID - newly server generated client HEX ID
* @return {Boolean} returns false is ID is found within clients array, true if its not found and its indeed unique
**/
function checkUniqueID(generatedID,clients){
  for (var i = 0; i < clients.length; i++) {
      if(clients[i].cid===generatedID){
          return false;
    }
  }
  return true;
}

module.exports = {clientHandler:clientHandler};
